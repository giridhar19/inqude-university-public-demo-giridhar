cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "cordova-plugin-device.device",
    "file": "plugins/cordova-plugin-device/www/device.js",
    "pluginId": "cordova-plugin-device",
    "clobbers": [
      "device"
    ]
  },
  {
    "id": "cordova-plugin-splashscreen.SplashScreen",
    "file": "plugins/cordova-plugin-splashscreen/www/splashscreen.js",
    "pluginId": "cordova-plugin-splashscreen",
    "clobbers": [
      "navigator.splashscreen"
    ]
  },
  {
    "id": "cordova-plugin-statusbar.statusbar",
    "file": "plugins/cordova-plugin-statusbar/www/statusbar.js",
    "pluginId": "cordova-plugin-statusbar",
    "clobbers": [
      "window.StatusBar"
    ]
  },
  {
    "id": "ionic-plugin-keyboard.keyboard",
    "file": "plugins/ionic-plugin-keyboard/www/android/keyboard.js",
    "pluginId": "ionic-plugin-keyboard",
    "clobbers": [
      "cordova.plugins.Keyboard"
    ],
    "runs": true
  },
  {
    "id": "cordova-plugin-email-composer.EmailComposer",
    "file": "plugins/cordova-plugin-email-composer/www/email_composer.js",
    "pluginId": "cordova-plugin-email-composer",
    "clobbers": [
      "cordova.plugins.email",
      "plugin.email"
    ]
  },
  {
    "id": "code-push.AcquisitionManager",
    "file": "plugins/code-push/script/acquisition-sdk.js",
    "pluginId": "code-push",
    "merges": [
      "window"
    ]
  },
  {
    "id": "cordova-plugin-dialogs.notification",
    "file": "plugins/cordova-plugin-dialogs/www/notification.js",
    "pluginId": "cordova-plugin-dialogs",
    "merges": [
      "navigator.notification"
    ]
  },
  {
    "id": "cordova-plugin-dialogs.notification_android",
    "file": "plugins/cordova-plugin-dialogs/www/android/notification.js",
    "pluginId": "cordova-plugin-dialogs",
    "merges": [
      "navigator.notification"
    ]
  },
  {
    "id": "cordova-plugin-code-push.codePush",
    "file": "plugins/cordova-plugin-code-push/bin/www/codePush.js",
    "pluginId": "cordova-plugin-code-push",
    "clobbers": [
      "codePush"
    ]
  },
  {
    "id": "cordova-plugin-code-push.localPackage",
    "file": "plugins/cordova-plugin-code-push/bin/www/localPackage.js",
    "pluginId": "cordova-plugin-code-push",
    "clobbers": [
      "LocalPackage"
    ]
  },
  {
    "id": "cordova-plugin-code-push.remotePackage",
    "file": "plugins/cordova-plugin-code-push/bin/www/remotePackage.js",
    "pluginId": "cordova-plugin-code-push",
    "clobbers": [
      "RemotePackage"
    ]
  },
  {
    "id": "cordova-plugin-code-push.syncStatus",
    "file": "plugins/cordova-plugin-code-push/bin/www/syncStatus.js",
    "pluginId": "cordova-plugin-code-push",
    "clobbers": [
      "SyncStatus"
    ]
  },
  {
    "id": "cordova-plugin-code-push.installMode",
    "file": "plugins/cordova-plugin-code-push/bin/www/installMode.js",
    "pluginId": "cordova-plugin-code-push",
    "clobbers": [
      "InstallMode"
    ]
  },
  {
    "id": "cordova-plugin-code-push.codePushUtil",
    "file": "plugins/cordova-plugin-code-push/bin/www/codePushUtil.js",
    "pluginId": "cordova-plugin-code-push",
    "runs": true
  },
  {
    "id": "cordova-plugin-code-push.fileUtil",
    "file": "plugins/cordova-plugin-code-push/bin/www/fileUtil.js",
    "pluginId": "cordova-plugin-code-push",
    "runs": true
  },
  {
    "id": "cordova-plugin-code-push.httpRequester",
    "file": "plugins/cordova-plugin-code-push/bin/www/httpRequester.js",
    "pluginId": "cordova-plugin-code-push",
    "runs": true
  },
  {
    "id": "cordova-plugin-code-push.nativeAppInfo",
    "file": "plugins/cordova-plugin-code-push/bin/www/nativeAppInfo.js",
    "pluginId": "cordova-plugin-code-push",
    "runs": true
  },
  {
    "id": "cordova-plugin-code-push.package",
    "file": "plugins/cordova-plugin-code-push/bin/www/package.js",
    "pluginId": "cordova-plugin-code-push",
    "runs": true
  },
  {
    "id": "cordova-plugin-code-push.sdk",
    "file": "plugins/cordova-plugin-code-push/bin/www/sdk.js",
    "pluginId": "cordova-plugin-code-push",
    "runs": true
  },
  {
    "id": "cordova-plugin-inappbrowser.inappbrowser",
    "file": "plugins/cordova-plugin-inappbrowser/www/inappbrowser.js",
    "pluginId": "cordova-plugin-inappbrowser",
    "clobbers": [
      "cordova.InAppBrowser.open",
      "window.open"
    ]
  },
  {
    "id": "cordova-plugin-actionsheet.ActionSheet",
    "file": "plugins/cordova-plugin-actionsheet/www/ActionSheet.js",
    "pluginId": "cordova-plugin-actionsheet",
    "clobbers": [
      "window.plugins.actionsheet"
    ]
  },
  {
    "id": "uk.co.workingedge.phonegap.plugin.launchnavigator.Common",
    "file": "plugins/uk.co.workingedge.phonegap.plugin.launchnavigator/www/common.js",
    "pluginId": "uk.co.workingedge.phonegap.plugin.launchnavigator",
    "clobbers": [
      "launchnavigator"
    ]
  },
  {
    "id": "uk.co.workingedge.phonegap.plugin.launchnavigator.LocalForage",
    "file": "plugins/uk.co.workingedge.phonegap.plugin.launchnavigator/www/localforage.v1.5.0.min.js",
    "pluginId": "uk.co.workingedge.phonegap.plugin.launchnavigator",
    "clobbers": [
      "localforage"
    ]
  },
  {
    "id": "uk.co.workingedge.phonegap.plugin.launchnavigator.LaunchNavigator",
    "file": "plugins/uk.co.workingedge.phonegap.plugin.launchnavigator/www/android/launchnavigator.js",
    "pluginId": "uk.co.workingedge.phonegap.plugin.launchnavigator",
    "merges": [
      "launchnavigator"
    ]
  },
  {
    "id": "cordova-plugin-firebase-analytics.FirebaseAnalytics",
    "file": "plugins/cordova-plugin-firebase-analytics/www/FirebaseAnalytics.js",
    "pluginId": "cordova-plugin-firebase-analytics",
    "merges": [
      "cordova.plugins.firebase.analytics"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-plugin-device": "2.0.1",
  "cordova-plugin-splashscreen": "4.1.0",
  "cordova-plugin-statusbar": "2.4.1",
  "cordova-plugin-whitelist": "1.3.3",
  "ionic-plugin-keyboard": "2.2.1",
  "cordova-plugin-email-composer": "0.8.15",
  "code-push": "2.0.4",
  "cordova-plugin-dialogs": "2.0.1",
  "cordova-plugin-code-push": "1.11.10",
  "cordova-plugin-inappbrowser": "1.7.2",
  "cordova-plugin-actionsheet": "2.3.3",
  "uk.co.workingedge.phonegap.plugin.launchnavigator": "4.2.0",
  "cordova-support-google-services": "1.0.0",
  "cordova-plugin-firebase-analytics": "0.12.1",
  "cordova-plugin-console": "1.1.0"
};
// BOTTOM OF METADATA
});