import { Injectable } from '@angular/core';
import { ApiService } from './apiService';
import { BaseClass } from './base';
import { HelperService } from '../helperService';

@Injectable()
export class Participants extends BaseClass {

  constructor(
    public apiService: ApiService,
    public helperService: HelperService
  ) {
    super(apiService);
    this.key = 'ParticipantId'
    this.url = 'Participants'
  }

  public markAttended(participantId: any, eventId: any) {

    return new Promise((resolve, reject) => {
      this.helperService.showLoading();
      let subURl = '/BarcodeApp/BarCodeReceiver?barcodevalue=' + participantId + '&HandshakeKey=Inqude_Barcode&eventid=' + eventId;
      let url = this.apiService.getUrl(subURl, 'barcode');
      this.apiService.ajax(url, 'string')
        .then((response: any) => {
          this.helperService.hideLoading();
          resolve(response._body);
        })
        .catch(() => {
          this.helperService.hideLoading();
          resolve();
        })
    });
  }
}
