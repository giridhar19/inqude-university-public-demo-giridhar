import { Injectable } from '@angular/core';
import { ApiService } from './apiService';
import { BaseClass } from './base';

@Injectable()
export class Speakers extends BaseClass {
  constructor(
    public apiService: ApiService
  ) {
    super(apiService);
    this.key = 'SpeakerId';
    this.url = 'Speakers'
  }
}
