import { Type } from '@angular/core';

export class PostItem {
  constructor(public component: Type<any>, public post: any) { }
}
