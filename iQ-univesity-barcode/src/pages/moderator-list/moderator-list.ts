import { Component, ViewChild } from '@angular/core';
import { Content, NavParams, PopoverController } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline';
import { HelperService } from '../../providers/helperService';
import { EventFilter } from '../../components/event-filter/event-filter';
import * as _ from 'underscore';

@Component({
  selector: 'moderator-list',
  templateUrl: 'moderator-list.html'
})

export class ModeratorList {
  @ViewChild(Content) eventList: Content;

  uid: any;
  events: any[] = [];
  segment: any = 'live';
  timeZone: any = 'America/Matamoros';
  fields: any = 'Name,EventId,EventStartDate,EventVenue,RegisteredParticipants,ParticipationCount';
  liveEvents: any = [];
  pastEvents: any = [];
  loadedEventsCount: number = 0;
  showLimit: number = 8;
  hideSearchBar: boolean = true;
  query: string = '';
  timeoutId: any;
  reachedMaxLimit: boolean = false;
  filterKeys: any = ['Name', 'EventVenue', 'Notes'];
  allEvents: any[] = [];
  hideBackButton: boolean = false;

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public helper: HelperService,
    public popoverCtrl: PopoverController,
    public navParams: NavParams
  ) {

    new Promise((resolve) => {
      if(!_.isEmpty(this.navParams.data)) {
        this.hideBackButton = true;
        resolve(this.navParams.get('uid'));
      } else {
        this.helper.getUid()
        .then((value:any) => {
          resolve(value)
        })
      }
    })
    .then((uid:any) => {
      this.uid = uid;
      let self = this;
      this.afoDatabase.list('/moderatorEvents/' + this.uid, { query: { orderByChild: 'EventStartDate' } })
        .subscribe((allEvents: any) => {
          this.events = [];
          this.loadedEventsCount = 0;
          let temp = _.partition(allEvents, function(event: any) {
            if (event.EventStartDate >= self.helper.getTimeStamp()) {
              return event.EventId;
            }
          });
          this.allEvents = [] = this.allEvents.concat(temp[0], temp[1]);
          this.liveEvents = temp[0];
          this.pastEvents = temp[1];
          this.loadEvents();
        });
    })

  }

  toggleNavBar() {
    this.hideSearchBar = !this.hideSearchBar;
  }

  onInput(){
    if (this.timeoutId) {
      clearTimeout(this.timeoutId)
    }
    this.timeoutId = setTimeout(() => {
      clearTimeout(this.timeoutId)
      this.loadedEventsCount = 0;
      this.events = this.allEvents;
    }, 500);
  }

  onIonClear() {
    this.query = '';
    this.events = [];
    this.loadedEventsCount = 0;
    this.toggleNavBar();
    this.loadEvents();
  }

  presentPopover(myEvent: any) {
    let popover = this.popoverCtrl.create(EventFilter, { filter: this.segment });
    popover.present({
      ev: myEvent
    });

    popover.onDidDismiss((result: any) => {
      if (result) {
        this.segment = result;
        this.toggleSelection();
      }
    })
  }

  toggleSelection() {
    this.eventList.scrollToTop();
    this.loadedEventsCount = 0;
    this.events = [];
    this.loadEvents();
  }

  getCurrentEvents() {
    return (this.segment == 'live') ? this.liveEvents : this.pastEvents;
  }

getEvents() {
    return new Promise((resolve) => {
      // get limited events
      let temp = this.getCurrentEvents();
      if(this.loadedEventsCount <  temp.length) {
        this.events = this.events.concat(temp.slice(this.loadedEventsCount, (this.showLimit + this.loadedEventsCount)));
        this.loadedEventsCount += this.events.length;
      } else {
        this.reachedMaxLimit = true;
      }
      resolve();
    })
  }

  loadEvents(ionRefresher?: any) {
    this.reachedMaxLimit = false;
    this.getEvents()
      .then(() => {
        if (ionRefresher) {
          ionRefresher.complete();
        }
      })
      .catch((err: any) => {
        console.log('err ')
      });
  }

}
