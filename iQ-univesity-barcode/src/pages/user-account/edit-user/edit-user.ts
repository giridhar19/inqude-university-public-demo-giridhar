import { Component } from '@angular/core';
import { AngularFire } from 'angularfire2';
import { App, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline';
import { HelperService } from '../../../providers/helperService';
import { UserData } from '../../../providers/user-data';
import { NavigationDashboardPage } from '../../navigation-dashboard/navigation-dashboard';

@Component({
  selector: 'page-user-datail-form',
  templateUrl: 'edit-user.html',
})

export class UserDetailFormPage {
  user: any = {};
  uid: string = '';
  isNewUser: any;
  university: any = {};
  constructor(
    public af: AngularFire,
    public events: Events,
    public afoDatabase: AngularFireOfflineDatabase,
    public app: App,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public helperService: HelperService,
    public userData: UserData
  ) {
    this.user = navParams.get('user') || {};
    this.isNewUser = navParams.get('isNewUser') || false;
  }

  saveUserDetails() {
    this.helperService.showLoading();

    let self = this;
    this.helperService.createUser({ email: this.user.email.toLowerCase(), password: this.user.password })
      .then((res: any) => {
        let userData: any = {
          firstName: this.user.firstName,
          lastName: this.user.lastName
        }

        self.af.database.object('/users/' + res.data.uid)
          .update(userData)
          .then(() => {
            let universityRef = self.af.database.list('/universities/').push({});
            return universityRef.set({
              name: self.university.name,
              id: universityRef.key,
              createdBy: res.data.uid
            });
          })
          .then(() => {
            self.helperService.showMessage('User details updated successfully');
            this.af.database.object('/users/' + res.data.uid)
              .subscribe((user: any) => {
                self.userData.login(user)
                  .then(() => {
                    self.helperService.hideLoading();
                    self.events.publish('user:login');
                    self.navCtrl.setRoot(NavigationDashboardPage)
                      .then(() => {
                        if (self.isNewUser) {
                          this.helperService.showMessage('Account created sucessfully', 2000);
                        }
                      });
                  })
              })
          })
      })
      .catch((errRes: any) => {
        if (errRes && errRes.error) {
          if (errRes.error.code == 'auth/invalid-password') {
            this.helperService.showMessage('Password must be atleast 6 characters');
          } else if (errRes.error.code == 'auth/email-already-exists') {
            this.helperService.showMessage('User already exists with this email address');
          }
        }
        this.helperService.hideLoading();
      });


  }

}
