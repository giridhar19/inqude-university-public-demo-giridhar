import { Component, ViewChild } from '@angular/core';
import { Content, NavController, NavParams, Searchbar } from 'ionic-angular';
import { EventDetailPage } from "../event-detail/event-detail";
import { HelperService } from '../../providers/helperService';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import * as moment from 'moment-timezone';
import { ApiService } from '../../providers/crm/apiService';
import { Talisma } from '../../providers/crm/talisma';

@Component({
  selector: 'page-event-list',
  templateUrl: 'event-list.html'
})
export class EventListPage {

  @ViewChild(Content) eventList: Content;

  events: any[] = [];
  segment: any = 'live';
  live: any = [];
  past: any = [];
  uid: any;
  roleValue: any;
  allEvents: any = [];
  loadedEvents: any[] = [];
  loadedEventsCount: number = 0;
  loadedLiveEventsCount: number = 0;
  loadedPastEventsCount: number = 0;
  eventsPerPage: number = 10;
  timeZone: string = 'America/Matamoros';
  reachedMaxLimit: boolean = false;
  query: string = '';
  hideSearchBar: boolean = true;
  eventIds: any[] = [];
  searchEnabled: boolean = false;
  subURI: string = '';
  timeoutId: any;
  hideBackButton: boolean;
  @ViewChild('searchBar') searchBar: Searchbar;

  constructor(
    public helperService: HelperService,
    private _nav: NavController,
    private navParams: NavParams,
    public apiService: ApiService,
    public afoDatabase: AngularFireOfflineDatabase,
    public talisma: Talisma
  ) {
    this.navParams.get('hideBackButton') ? this.hideBackButton = this.navParams.get('hideBackButton') : this.hideBackButton = true;
    this.init();
  }

  init(ionRefresher?: any) {
    let self = this;
    if (!ionRefresher) {
      this.helperService.showLoading();
    }
    new Promise((resolve) => {
      this.helperService.getRoleValue()
        .then((roleValue: any) => {
          self.roleValue = roleValue;
          resolve();
        });
    })
      .then(() => {
        if (this.roleValue == 99) {
          let temp = { loadedLiveEventsCount: this.loadedLiveEventsCount, loadedPastEventsCount: this.loadedPastEventsCount, loadedEvents: this.loadedEvents };
          this.loadedLiveEventsCount = 0;
          this.loadedPastEventsCount = 0;
          this.getAllEvents()
            .then(() => {
              if (ionRefresher) {
                ionRefresher.complete();
              }
              else {
                this.helperService.hideLoading();
              }
            })
            .catch((err) => {
              console.log('err occured', err);
              this.helperService.hideLoading();
              this.loadedLiveEventsCount = temp.loadedLiveEventsCount;
              this.loadedPastEventsCount = temp.loadedPastEventsCount;
              this.loadedEvents = temp.loadedEvents;
            })
            ;
        }
      })
  }

  onIonClear() {
    this.query = '';
    this.searchEnabled = false;
    this.reachedMaxLimit = false;
    this.loadedEvents = [];
    this.loadedEventsCount = 0;
    this.loadedLiveEventsCount = 0;
    this.loadedPastEventsCount = 0;
    this.toggleNavBar();
    this.loadEvents();
  }

  toggleNavBar() {
    this.hideSearchBar = !this.hideSearchBar;
    setTimeout(() => {
      if (this.searchBar) {
        this.searchBar.setFocus();
      }
    })
  }

  getAllEvents() {
    return new Promise((resolve) => {
      this.getEvents(this.segment)
        .then((response: any) => {
          if (response.length) {
            this.live = response;
            this.loadedEvents = response;
          }
          resolve();
        });
    })

  }

  updateList(segment: any) {
    this.reachedMaxLimit = false;
    if (!this.hideSearchBar) {
      this.query = '';
      this.searchEnabled = false;
      this.toggleNavBar();
    }
    if (segment.value == 'live') {
      this.loadedEvents = this.live;
      this.loadedLiveEventsCount = this.loadedEvents.length;
    }
    else if (segment.value == 'past') {
      this.helperService.showLoading();
      this.getEvents('past')
        .then((events: any) => {
          this.past = events;
          this.loadedPastEventsCount = events.length;
          this.loadedEvents = this.past;
          this.helperService.hideLoading();
        })
        .catch((err) => {
          this.helperService.hideLoading();
          console.log('err occured', err);
        });
    }
    this.eventList.scrollToTop();
  }

  public viewEvent(eventData: any) {
    this._nav.push(EventDetailPage, { event: eventData });
  }

  loadEvents(infiniteScroll?: any) {
    setTimeout(() => {
      this.getEvents(this.segment, this.searchEnabled ? this.subURI : '')
        .then((events: any) => {
          this.loadedEvents = this.loadedEvents.concat(events);
          if (infiniteScroll) {
            infiniteScroll.complete();
          }
        })
    }, 100);
  }

  onInput() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId)
    }
    this.timeoutId = setTimeout(() => {
      clearTimeout(this.timeoutId);
      if (this.query && this.query.length) {
        this.loadedEvents = [];
        this.reachedMaxLimit = false;
        this.loadedLiveEventsCount = 0;
        this.loadedPastEventsCount = 0;
        this.helperService.showLoading();
        this.searchEnabled = true;
        this.subURI = " and contains(Name,'" + this.query + "')";
        this.getEvents(this.segment, this.subURI)
          .then((events: any) => {
            this.loadedEvents = events;
            this.helperService.hideLoading();
          })
      }
      else {
        this.reachedMaxLimit = false;
        this.loadEvents();
      }
    }, 500);
  }

  getEvents(segment: string, query?: string) {
    return new Promise((resolve, reject) => {
      let subUrl = '$filter=(EventStartDate' + (segment == 'live' ? ' ge ' : ' lt ') + (moment().tz(this.timeZone).subtract(1, 'days').endOf('day').format('') + ") ");
      subUrl = subUrl.concat(((query && query.length) ? "" + query + "&" : "&") + '$orderby=EventStartDate' + (segment == 'live' ? ' asc' : ' desc '));
      this.talisma.events.getEvents(this.eventsPerPage, segment == 'live' ? this.loadedLiveEventsCount : this.loadedPastEventsCount, subUrl)
        .then((response: any) => {
          if (response.length) {
            segment == 'live' ? this.loadedLiveEventsCount += response.length : this.loadedPastEventsCount += response.length
            resolve(response)
          }
          else {
            this.reachedMaxLimit = true;
            resolve([]);
          }
        })
        .catch((err) => {
          console.log('err occured', err);
          reject(err)
        });
    });
  }

  getClass(event: any) {
    return ((moment(event.EventStartDate).tz(this.timeZone).format('x')) >= (moment().tz(this.timeZone).subtract(1, 'days').endOf('day').format('x'))) ? '' : 'past-event';
  }
}
