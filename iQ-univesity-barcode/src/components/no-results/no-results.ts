import { Component, Input } from '@angular/core';
@Component({
  selector: 'no-results',
  templateUrl: 'no-results.html'
})

export class NoResults {
  @Input('icon') icon: string;
  @Input('text') text: string;
  constructor() { }
}
