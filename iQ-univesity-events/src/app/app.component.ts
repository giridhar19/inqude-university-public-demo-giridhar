import { Component, ViewChild } from "@angular/core";
import { AngularFire } from "angularfire2";
import { AlertController, App, Events, IonicApp, LoadingController, MenuController, Nav, Platform, ToastController } from "ionic-angular";
import { AngularFireOfflineDatabase } from "angularfire2-offline/database";

import { UserData } from "../providers/user-data";
import { MyEventsPage } from "../pages/my-events/my-events";
import { Talisma } from "../providers/crm/talisma";
import { NavigationDashboardPage } from "../pages/navigation-dashboard/navigation-dashboard";
import { EditModerator } from "../components/moderator/edit-moderator";
import { LoginPage } from "../pages/login/login";

import { Device } from "@ionic-native/device";
import { Splashscreen } from "ionic-native";
import { Storage } from "@ionic/storage";
import { StatusBar } from "@ionic-native/status-bar";
import { CodePush } from "@ionic-native/code-push";
import { Observable } from "rxjs/Rx";
import { HelperService } from "../providers/helperService";
import { LinkService } from "../providers/linkService";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { Push, PushObject } from "@ionic-native/push";
import { NotificationsListPage } from "../pages/notification-list/notification-list";
import * as firebase from "firebase";

import { PortalConfiguration } from "../constants/portal-config";
import * as $ from 'jquery';
declare var cordova: any;

export interface PageInterface {
  title: string;
  component?: any;
  icon: string;
  logsOut?: boolean;
  hideFor?: number;
  index?: number;
  logsIn?: boolean;
}

@Component({
  templateUrl: "app.template.html"
})
export class UOAEvents {
  // the root nav is a child of the root app component
  // @ViewChild(Nav) gets a reference to the app's root nav
  @ViewChild(Nav) nav: Nav;

  // List of pages that can be navigated to from the left menu
  // the left menu only works after login
  // the login page disables the left menu

  loggedInPages: PageInterface[] = [
    { title: "My Events", component: MyEventsPage, icon: "star" },
    { title: "Account", component: EditModerator, icon: "contact" },
    { title: "Notifications", component: NotificationsListPage, icon: "alert" },
    { title: "Logout", icon: "log-out", logsOut: true }
  ];

  loggedOutPages: PageInterface[] = [{ title: "Login", icon: "log-in", component: LoginPage }];

  menuPages: any = [];
  accountPages: PageInterface[] = this.loggedOutPages;
  notifications: any[] = [];
  rootPage: any;
  loading: any;
  roleValue: any;
  user: any = null;
  universityId: string;
  isCordovaEnabled: boolean;
  lastTimeBackPress = 0;
  timePeriodToExit: number = 3000;
  showMenu: boolean = true;
  hasLoggedIn: boolean = false;
  userSubsciption: any;

  constructor(
    public af: AngularFire,
    public events: Events,
    public afoDatabase: AngularFireOfflineDatabase,
    public alertCtrl: AlertController,
    public app: App,
    private codePush: CodePush,
    public device: Device,
    public helper: HelperService,
    public ionicApp: IonicApp,
    public link: LinkService,
    public inAppBrowser: InAppBrowser,
    public loadingCtrl: LoadingController,
    public menu: MenuController,
    public platform: Platform,
    public push: Push,
    private statusBar: StatusBar,
    public storage: Storage,
    public talisma: Talisma,
    public toastCtrl: ToastController,
    public userData: UserData,
    public portalConfig: PortalConfiguration // public fb: FB
  ) {
    let self: any = this;
    this.loading = loadingCtrl.create({
      content: "Loading ..."
    });
    this.loading.present();

    this.isCordovaEnabled = this.device.cordova !== null;

    this.storage
      .ready()
      .then(() => {
        this.statusBar.backgroundColorByHexString("#1e407c");
        return new Promise((resolve) => {
          this.storage
            .get("hasLoggedIn")
            .then((hasLoggedIn) => {
              this.hasLoggedIn = hasLoggedIn;
              this.getUser();
              return this.helper.updateUniversityId();
            })
            .then(() => {
              return this.helper.getUniversityId();
            })
            .then((universityId: string) => {
              this.universityId = '/universities/' + universityId;
              return this.helper.getRoleValue();
            })
            .then((roleValue) => {
              if (this.hasLoggedIn) {
                self.showMenu = true;
                this.enableMenu(true);
                self.rootPage = NavigationDashboardPage;
              } else {
                this.showMenu = false;
                self.rootPage = LoginPage;
              }
              resolve();
            });
        });
      })
      .then(() => {
        self.platformReady().then(() => {
          self.loading.dismiss();
          self.nav.setRoot(self.rootPage);

          if (self.isCordovaEnabled) {
            self.codepushSync();
            //Executes at every 30 minutes interval
            Observable.interval(1000 * 60 * 30).subscribe((x) => {
              self.codepushSync();
            });
          }
        });
      })
      .catch((err) => {
        console.log("err occured" + err);
      });

    // decide which menu items should be hidden by current login status stored in local storage
    this.userData.hasLoggedIn().then((hasLoggedIn) => {
      this.enableMenu(hasLoggedIn === true);
      this.getUser();
    });

    this.listenToLoginEvents();
  }

  changePicture() {
    $('#attachProfilePicture').click();
  }

  saveUrl(pictureObj: any) {
    this.helper.saveProfilePic(pictureObj.url);
  }

  codepushSync() {
    //Codpush checks for new updates & downloads if any new updates are available
    this.codePush.sync().subscribe((syncStatus: any) => {
      if (syncStatus == 1) {
        let alert = this.alertCtrl.create({
          title: "Restart app",
          message: "New Updates Installed & requires app to be restarted",
          buttons: [
            {
              text: "Restart",
              handler: () => {
                this.codePush.restartApplication();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        alert.present();
      }
    });
  }

  getUser() {
    if (this.user !== null) {
      return false;
    }
    this.userData.getUser().then((userObservable: any) => {
      this.userSubsciption = userObservable.subscribe((user: any) => {
        if (!user.$exists()) {
          return false;
        }
        this.user = user;
        this.storage.get("roleValue").then((roleValue: any) => {
          if (this.user.roleValue !== roleValue) {
            this.storage.set("roleValue", this.user.roleValue).then(() => {
              this.userData.hasLoggedIn().then((hasLoggedIn: any) => {
                this.enableMenu(hasLoggedIn === true);
              });
            });
          }
        });
        if (user.disabled == true) {
          this.logout();
          this.toastCtrl
            .create({
              message: "User has been blocked by the Admin, please contact Admin for details",
              duration: 3000
            })
            .present();
        }
      });
    });
  }

  logout() {
    setTimeout(() => {
      this.userSubsciption.unsubscribe();
      this.af.auth.logout();
      this.userData.logout();
      this.showMenu = false;
      this.nav.setRoot(LoginPage);
      this.enableMenu(false);
    }, 100);
  }

  authenticateUser() {
    let tempData: any = { user: {} };

    this.mockLoginView()
      .then((id: any) => {
        this.helper.showLoading();
        return this.helper.createCustomToken(id);
      })
      .then((response: any) => {
        if (response.user && !response.user.isNewUser) {
          this.login(response, "You have successfully logged in");
        } else {
          tempData.token = response.token;
          tempData.user.roleValue = response.user.roleValue;
          tempData.user.uid = response.user.uid;
          return this.getContact(tempData.user.uid);
        }
      })
      .then((response: any) => {
        if (!response) {
          return false;
        }
        tempData.user.email = response[0].Email;
        tempData.user.name = response[0].Name;
        this.afoDatabase
          .object("users/" + tempData.user.uid)
          .set(tempData.user)
          .then(() => {
            this.login(tempData, "Welcome to Inqude University Event app");
          });
      })
      .catch((error: any) => {
        this.helper.hideLoading();
        console.log("err ", error);
        this.helper.showMessage("Failed to login, try later");
      });
  }

  // load olli portal to authenticate user
  mockLoginView() {
    return new Promise((resolve) => {
      let browser = this.inAppBrowser.create(this.portalConfig.getLoginUrl(), "_blank", this.helper.getBrowserSettings());

      // remove menu icon in olli portal
      browser.on("loadstop").subscribe((_event: any) => {
        browser.executeScript({
          code: "var el = document.getElementsByClassName('navbar-toggle collapsed')[0];  el.parentElement.removeChild(el);"
        });
      });

      browser.on("loadstart").subscribe((_event: any) => {
        if (_event.url.includes("success.html")) {
          let id = _event.url.split("contactid=")[1];
          browser.close();
          resolve(id);
        } else if (_event.url == "https://ccsnoncredittest.ua.edu/CustomerRegistration/") {
          // problem getting ContactId
          browser.close();
          this.helper.showMessage("Failed to login");
        }
      });
    });
  }

  // get participant basic info from crm
  getContact(contactId: any) {
    return this.talisma.contacts.findBy({ url: "?$filter=ContactId eq " + contactId + "&$select=Name,Email" });
  }

  openPage(page: PageInterface) {
    // the nav component was found using @ViewChild(Nav)
    // reset the nav to remove previous pages and only have this page
    // we wouldn't want the back button to show in this scenario
    // if (page.logsIn === true) {
    //   this.authenticateUser();
    //   return false;
    // }
    console.log(page)
    if (page.logsOut == true) {
      this.logout();
      return false;
    }
    if (page.index) {
      this.nav.setRoot(page.component, { tabIndex: page.index });
    } else {
      this.nav.setRoot(page.component).catch(() => {
        console.log("Didn't set nav root");
      });
    }
    // this.link.viewPage(page);
  }

  viewPage(page: any) {
    this.link.viewPage(page);
  }

  listenToLoginEvents() {
    this.events.subscribe("user:login", () => {
      this.showMenu = true;
      this.enableMenu(true);
      this.getUser();
    });

    this.events.subscribe("user:logout", () => {
      this.showMenu = false;
      this.enableMenu(false);
      this.user = null;
    });
  }

  enableMenu(loggedIn: boolean) {
    this.afoDatabase.list(this.universityId + "/menus/SIDE_MENU/")
      .subscribe((snapshots: any) => {
        this.menuPages = snapshots;
      });
    this.storage.get("roleValue").then((roleValue: any) => {
      this.roleValue = roleValue;
      if (loggedIn) {
        this.accountPages = this.loggedInPages;
      } else {
        this.accountPages = this.loggedOutPages;
      }
    });
  }

  platformReady() {
    return new Promise((resolve, reject) => {
      let self: any = this;

      this.platform.ready().then(() => {
        if (this.isCordovaEnabled && (this.platform.is("android") || this.platform.is("ios"))) {
          this.push.hasPermission().then((res: any) => {
            if (res.isEnabled) {
              console.log("We have permission to send push notifications");
            } else {
              console.log("We do not have permission to send push notifications");
            }
          });

          self.afoDatabase
            .object(this.universityId + "/settings/icon")
            .take(1)
            .subscribe((icon: any) => {
              let options = {
                android: {
                  icon: icon.name,
                  iconColor: icon.eventAppColor,
                  senderID: "961245762123",
                  forceShow: true
                },
                ios: {
                  senderID: "961245762123",
                  gcmSandbox: true,
                  alert: "true",
                  badge: true,
                  sound: "false"
                },
                windows: {},
                browser: {
                  pushServiceURL: "http://push.api.phonegap.com/v1/push"
                }
              };
              let pushObject: PushObject = self.push.init(options);

              pushObject.on("error").subscribe((error: any) => console.error("Error with Push plugin", error));
              pushObject.on("registration").subscribe((registration: any) => {
                console.log("Device registered", registration);
                self.userData.hasLoggedIn().then((hasLoggedIn: any) => {
                  if (hasLoggedIn) {
                    self.userData.getUid().then((uid: any) => {
                      self.af.database.object("/notificationTokens/" + uid).set({
                        token: registration.registrationId,
                        type: this.platform.is("android") && !this.platform.is("core") ? "android" : "ios"
                      });
                      this.userData.setFCMToken(registration.registrationId);
                    });
                  } else {
                    this.userData.setFCMToken(registration.registrationId);
                  }
                });
              });

              pushObject.on("notification").subscribe((notification: any) => {
                let data = notification.additionalData;
                if (!data.foreground) {
                  //when the notification is tapped in the notification tray
                  //route to the Specific pages
                  if (!this.platform.is("core") && this.platform.is("android")) {
                    data.route = typeof data.routes == "string" ? JSON.parse(data.routes) : data.routes;
                  } else if (!this.platform.is("core") && this.platform.is("ios")) {
                    data.routes =
                      typeof data["gcm.notification.routes"] == "string"
                        ? JSON.parse(data["gcm.notification.route"])
                        : data["gcm.notification.route"];
                  }
                  if (data.route) {
                    this.link.viewPage(data.route);
                  } else {
                    this.nav.push(NavigationDashboardPage);
                  }
                }
              });

              if (!this.platform.is("core")) {
                //sets the FCM token to DB once the user loggedIn
                this.af.auth.subscribe((auth) => {
                  if (auth) {
                    let uid = auth.uid;
                    this.userData.getFCMToken().then((token) => {
                      if (token) {
                        this.af.database
                          .object("/notificationTokens/" + uid)
                          .set({ token: token, type: this.platform.is("android") && !this.platform.is("core") ? "android" : "ios" });
                      }
                    });
                  }
                });
              }
            });

          this.platform.registerBackButtonAction(() => {
            let activePortal =
              this.ionicApp._loadingPortal.getActive() ||
              this.ionicApp._modalPortal.getActive() ||
              this.ionicApp._toastPortal.getActive() ||
              this.ionicApp._overlayPortal.getActive();

            if (activePortal) {
              activePortal.dismiss();
              return;
            }
            if (this.menu.isOpen()) {
              this.menu.close();
              return;
            }
            if (
              this.nav.getActive() &&
              (this.nav.getActive().pageRef().nativeElement.tagName !== "PAGE-NAVIGATION-DASHBOARD" && this.nav.canGoBack())
            ) {
              this.nav.pop();
              return;
            }
            if (
              this.nav.getActive() &&
              (this.nav.getActive().pageRef().nativeElement.tagName !== "PAGE-NAVIGATION-DASHBOARD" && !this.nav.canGoBack())
            ) {
              this.nav.setRoot(NavigationDashboardPage);
              return;
            }
            if (this.nav.getActive() && this.nav.getActive().pageRef().nativeElement.tagName == "PAGE-NAVIGATION-DASHBOARD") {
              if (+new Date() - this.lastTimeBackPress < this.timePeriodToExit) {
                this.platform.exitApp();
              } else {
                this.toastCtrl
                  .create({
                    message: "Press back again to exit App",
                    duration: 3000,
                    position: "bottom"
                  })
                  .present();
                this.lastTimeBackPress = +new Date();
              }
            }
          });
        }
        Splashscreen.hide();
        resolve();
      });
    });
  }

  isActive(menuItem: any) {
    let activeComponent = this.nav.getActive();
    if (!activeComponent || !activeComponent.component) {
      return "";
    }
    let isComponentPage: boolean = activeComponent.component.name && activeComponent.component.name === menuItem.value;
    let isCustomePage: boolean =
      activeComponent.component.name &&
      activeComponent.component.name === "CustomPage" &&
      activeComponent.data &&
      activeComponent.data.pageId === menuItem.value;

    if (isComponentPage || isCustomePage) {
      return "primary";
    }
    return "";
  }

  login(responseData: any, msg: any) {
    let self: any = this;
    firebase
      .auth()
      .signInWithCustomToken(responseData.token)
      .then((firebaseUser) => {
        return self.userData.login(responseData.user);
      })
      .then(() => {
        self.events.publish("user:login");
        self.helper.hideLoading();
        self.helper.showMessage(msg, 3000, "showicon");
        self.nav.setRoot(NavigationDashboardPage);
      })
      .catch((err: any) => {
        this.helper.hideLoading();
        if (err.code && typeof err.code == "string" && err.code.includes("auth/")) {
          self.helper.showMessage("Failed to login," + err.message);
        } else {
          self.helper.showMessage("Failed to login, please try after some time");
        }
        console.log("Err occured", err);
      });
  }
}
