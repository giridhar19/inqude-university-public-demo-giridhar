import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { Events } from 'ionic-angular';

@Injectable()
export class UserData {
  HAS_LOGGED_IN = 'hasLoggedIn';
  uid: string;

  constructor(
    public storage: Storage,
    public afoDatabase: AngularFireOfflineDatabase,
    public events: Events
  ) { }

  login(user: any) {
    return new Promise((resolve, reject) => {
      this.storage.set(this.HAS_LOGGED_IN, true)
        .then(() => {
          return this.setUid(user.uid);
        })
        .then(() => {
          return this.storage.set("roleValue", user.roleValue);
        })
        .then(() => {
          if (user.firstName) {
            this.setUsername(user.firstName + ' '+ (user.lastName ? user.lastName : ''))
              .then(() => {
                resolve()
              })
          } else {
            resolve()
          }
        })
    })

  };

  signup(username: string): void {
    this.storage.set(this.HAS_LOGGED_IN, true);
    this.setUsername(username);
  };

  logout(): void {
    this.storage.remove(this.HAS_LOGGED_IN).then(() => {
      return this.storage.remove('username');
    })
      .then(() => {
        return this.storage.remove('username');
      })
      .then(() => {
        return this.storage.remove('roleValue');
      })
      .then(() => {
        return this.storage.remove('uid');
      })
      .then(() => {
        this.events.publish('user:logout');
      })
      .catch((err) => {
        console.log('failed to set the values to the local storage');
      })
  };

  setUsername(username: string) {
    return this.storage.set('username', username);
  };

  getUsername(): Promise<string> {
    return this.storage.get('username')
      .then((value: any) => {
        return value;
      });
  };

  getRoleValue(): Promise<number> {
    return this.storage.get('roleValue')
      .then((value: any) => {
        return value;
      });
  }

  setRoleValue(): Promise<number> {
    return this.storage.set('roleValue', '0')
  }

  hasLoggedIn(): Promise<boolean> {
    return this.storage.get(this.HAS_LOGGED_IN)
      .then((value: any) => {
        return value === true;
      });
  };

  setAsLoggedIn() {
    return this.storage.set('hasLoggedIn', true);
  };

  setUid(uid: string): Promise<any> {
    return this.storage.set('uid', uid).then(() => {
      return 'sucess';
    })
      .catch(() => {
        return 'failed';
      })
  }

  // Get Info of Single User
  getUser() {
    // Getting UID of Logged In User
    return this.getUid().then(uid => {
      return this.afoDatabase.object(`/users/${uid}`);
    });
  }

  getUid(): Promise<string> {
    return this.storage.get('uid')
      .then((value: any) => {
        return value;
      })
  }

  setFCMToken(token: string) {
    return this.storage.set('fcmToken', token).then(() => {
      return 'sucess';
    })
      .catch(() => {
        return 'failed'
      })
  }

  getFCMToken() {
    return this.storage.get('fcmToken').then((value) => {
      return value;
    })
  }
}
