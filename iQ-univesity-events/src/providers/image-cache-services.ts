import { Injectable } from '@angular/core';
import * as ImgCache from 'imgcache.js';

@Injectable()
export class ImageCacheService {

  constructor(
  ) { }

  isCached(src: string) {
    return new Promise((resolve, reject) => {
      ImgCache.isCached(src, (path: string, sucess: any) => {
        resolve(sucess ? true : false);
      })
      resolve(false);
    })
  }

  cacheFile(src: string) {
    ImgCache.cacheFile(src);
  }

  getCachedFileURL(src: string) {
    return new Promise((resolve, reject) => {
      ImgCache.getCachedFileURL(src, (path: string, url: any) => {
        resolve(url)
      });
    })

  }
}
