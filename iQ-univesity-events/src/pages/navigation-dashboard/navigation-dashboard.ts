import { Component } from "@angular/core";
import { MenuController, NavController, NavParams } from "ionic-angular";
import { UserData } from "../../providers/user-data";
import { AngularFireOfflineDatabase } from "angularfire2-offline/database";
import { FirebaseAnalyticsProvider } from "../../providers/firebase-analytics-provider";
import { StatusBar } from "@ionic-native/status-bar";
import { HelperService } from "../../providers/helperService";
import { Talisma } from "../../providers/crm/talisma";
import { HomePage } from "../home/home";
import * as _ from "underscore";

@Component({
  selector: "page-navigation-dashboard",
  templateUrl: "navigation-dashboard.html",
  providers: [FirebaseAnalyticsProvider]
})
export class NavigationDashboardPage {
  slides: any[];
  hasLoggedIn: boolean;
  menuItems: any[] = [];
  title: string;
  events: any[] = [];
  universityId: string;
  eventListPage: any = {
    icon: "calendar",
    title: "Events",
    type: "COMPONENT",
    value: "EventListPage"
  };
  featuredContentPerPage: number;
  posts: any = [];
  isProcessing: boolean = true;

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public firebaseAnalytics: FirebaseAnalyticsProvider,
    public menuCtrl: MenuController,
    private navCtrl: NavController,
    public navParams: NavParams,
    private statusBar: StatusBar,
    public userdata: UserData,
    public helper: HelperService,
    public talisma: Talisma
  ) {
    this.menuCtrl.enable(true);
    this.isProcessing = true;
    this.statusBar.backgroundColorByHexString("#1e407c");
    this.title = navParams.get("title") ? navParams.get("title") : "Home";
    firebaseAnalytics.setScreen("NAVIGATION_DASHBOARD");
    this.helper.getUniversityId()
      .then((universityId) => {
        this.universityId = 'universities/' + universityId + '/';
        // homesliders url from db
        this.afoDatabase.object(this.universityId + "/settings/homePage", { preserveSnapshot: true }).subscribe((snapshot: any) => {
          this.slides = snapshot ? snapshot : [];
        });
        this.userdata.hasLoggedIn().then((hasLoggedIn) => {
          this.hasLoggedIn = hasLoggedIn;
        });
        //fetches the menus in homepage
        this.afoDatabase.list(this.universityId + "/menus/HOME_PAGE/").subscribe((menuItems) => {
          this.menuItems = menuItems ? menuItems : [];
        });
        //count to display featuredContentPerPage
        this.afoDatabase.object(this.universityId + "/settings/featuredContentPerPage", { preserveSnapshot: true }).subscribe((data) => {
          this.featuredContentPerPage = data.$value ? data.$value : 5;
          //get the posts
          this.afoDatabase.list(this.universityId + "/posts", { query: { orderByChild: "isFeaturedContent", equalTo: true } }).subscribe((posts: any) => {
            let tempPosts: any = _.sortBy(
              _.filter(posts, (post: any) => {
                return post.published == true;
              }),
              (node: any) => {
                return -new Date(node.modifiedDate).getTime();
              }
            );
            this.posts = tempPosts.slice(0, this.featuredContentPerPage);
            this.isProcessing = false;
          });
        });
        let subUrl = "?$select=Name,EventStartDate&$filter=(EventStartDate ge " + this.helper.getTimeStamp() + ") &$orderby=EventStartDate asc";

        this.talisma.events
          .getEvents(2, 0, subUrl)
          .then((response: any) => {
            this.events = response;
          })
          .catch((err) => {
            console.log("err occured", err);
          });

      });
  }

  getTriplets() {
    let triplets: any[] = [];
    let length = this.menuItems.length;
    for (let i = 0; i < length; i += 3) {
      let triples: any[] = [];
      triples.push(this.menuItems[i]);
      if (i + 1 < length) {
        triples.push(this.menuItems[i + 1]);
      }
      if (i + 2 < length) {
        triples.push(this.menuItems[i + 2]);
      }
      triplets.push(triples);
    }
    return triplets;
  }

  openHomePage() {
    this.navCtrl.push(HomePage);
  }

  // ngOnDestroy() {
  //   this.statusBar.backgroundColorByHexString("#1e407c");
  // }
}
