import { Component, ViewChild } from '@angular/core';
import { AngularFire } from 'angularfire2';
import { App, Content, Events, MenuController, ModalController, NavController, NavParams, Platform } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { UserData } from '../../providers/user-data';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { FirebaseAnalyticsProvider } from '../../providers/firebase-analytics-provider';
import { NotificationsListPage } from '../notification-list/notification-list';
import { HelperService } from "../../providers/helperService";
import * as _ from 'underscore';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [FirebaseAnalyticsProvider]
})

export class HomePage {
  slides: any;
  roleValue: number;
  badgeNumber: number;
  posts: any = [];
  loadedPosts: any = [];
  loadedPostsCount: number;
  postsPerPage: number;
  isNewPostExist: boolean = false;
  totalPostsCount: number;
  universityId: string;
  title: string;
  @ViewChild(Content) content: Content;

  constructor(
    public app: App,
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    public firebaseAnalytics: FirebaseAnalyticsProvider,
    public helper: HelperService,
    public modalCtrl: ModalController,
    public menuCtrl: MenuController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public userdata: UserData,
    public events: Events,
    public platform: Platform
  ) {
    // eanbling menu
    menuCtrl.enable(true);
    this.title = this.navParams.get('title') ? this.navParams.get('title') : 'Activity Stream';
    firebaseAnalytics.setScreen('ACTIVITY_STREAM');
    this.helper.getUniversityId()
      .then((universityId: string) => {
        this.universityId = '/universities/' + universityId;
        // homesliders url from db
        this.af.database.list(this.universityId + '/images/homepage_sliders', { preserveSnapshot: true })
          .subscribe((snapshots: any) => {
            let temp: any[] = [];
            for (let snapshot of snapshots) {
              temp.push(snapshot.val());
            }
            this.slides = temp;
          });

        this.afoDatabase.object(this.universityId + "/settings/postsPerPage")
          .take(1)
          .subscribe((postsPerPage) => {
            this.afoDatabase.list(this.universityId + '/posts', { query: { orderByChild: 'published', equalTo: true } })
              .subscribe((posts) => {
                console.log(posts)
                this.loadedPostsCount = this.postsPerPage = postsPerPage.$value ? postsPerPage.$value : 10;
                if (posts.length > 0) {
                  this.posts = _.sortBy(posts, 'publishedTime').reverse();
                  if (!this.totalPostsCount) {
                    this.loadedPosts = this.posts.slice(0, this.postsPerPage);
                    this.totalPostsCount = this.posts.length;
                  }
                  else {
                    if (this.posts.length > this.totalPostsCount) {
                      this.isNewPostExist = true;
                      this.totalPostsCount = this.posts.length;
                    }
                    else {
                      let removedItem = this.loadedPosts.filter(this.comparer(this.posts))[0];
                      for (let i = 0; i < this.loadedPosts.length; i++) {
                        if (removedItem && removedItem.id == this.loadedPosts[i].id) {
                          this.loadedPosts.splice(i, 1);
                        }
                      }
                      this.totalPostsCount = this.posts.length;
                    }
                  }
                }
                else {
                  this.posts = [];
                }
              })
          })

        // remove the 'post', if associated Calendar session doesn't exists
        this.events.subscribe('delete:post', (postId: string) => {
          this.afoDatabase.object(this.universityId + '/posts/' + postId).remove();
        });
      })
  }

  ionViewWillEnter() {
    //get roleValue from db
    this.userdata.getRoleValue()
      .then((value) => {
        this.roleValue = value;
      })
  }

  comparer(otherArray: any) {
    return function(current: any) {
      return otherArray.filter(function(other: any) {
        return other.id == current.id;
      }).length == 0;
    }
  }

  updatePosts() {
    this.loadedPostsCount = this.postsPerPage;
    this.loadedPosts = this.posts.slice(0, this.postsPerPage);
    this.totalPostsCount = this.posts.length;
    this.isNewPostExist = false;
    this.content.scrollToTop();
  }

  loadPosts(infiniteScroll: any) {
    setTimeout(() => {
      for (let i = 0; i < this.postsPerPage; i++) {
        if (this.loadedPostsCount < this.posts.length && this.posts[this.loadedPostsCount]) {
          this.loadedPosts.push(this.posts[this.loadedPostsCount]);
          this.loadedPostsCount++;
        }
      }
      infiniteScroll.complete();
    }, 100);
  }

  login() {
    this.app.getRootNav().setRoot(LoginPage);
  }

  setImageUrl(imageUrl: any) {
    return 'url(' + imageUrl + ');'
  }

  viewNotifications() {
    this.navCtrl.push(NotificationsListPage);
  }
}
