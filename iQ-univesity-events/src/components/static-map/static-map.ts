import { Component, Input } from '@angular/core';
import { Platform } from 'ionic-angular';
import { LaunchNavigator } from '@ionic-native/launch-navigator';

@Component({
  selector: 'static-map',
  templateUrl: 'static-map.html'
})

export class StaticMap {
  @Input('params') venue: any;
  mapUrl: any;
  deviceWidth: any;
  deviceHeight: any;

  constructor(
    private launchNavigator: LaunchNavigator,
    public platform: Platform
  ) {
    this.deviceWidth = this.platform.width();
    this.deviceHeight = '180';
  }

  ngOnChanges() {
    this.mapUrl = 'https://maps.googleapis.com/maps/api/staticmap?sensor=false&size=' + this.deviceWidth + 'x' +
        this.deviceHeight + '&visual_refresh=true&scale=2&center=' + this.venue + '&zoom=18&maptype=roadmap&markers='+this.venue;
  }

  viewLocation() {
    if (this.platform.is('ios') || this.platform.is('android')) {
      this.launchNavigator.navigate(this.venue)
        .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
        );
    }
    if (this.platform.is('core')) {
      window.open('https://www.google.co.in/maps/place/@' + this.venue + ',17z/', '_blank', 'location=yes');
    }
  }

}
